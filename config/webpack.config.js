const path = require('path');
const resolve = require('resolve');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin-alt');
const typescriptFormatter = require('react-dev-utils/typescriptFormatter');

const webpack = require('webpack');
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');
const WatchMissingNodeModulesPlugin = require('react-dev-utils/WatchMissingNodeModulesPlugin');

const regex = {
  typescript: /\.(ts|tsx)$/,
  css: /\.css$/,
  fonts: /\.woff2?$/,
};

const paths = {
  src: path.resolve(__dirname, '../src'),
  styleguidist: path.resolve(__dirname, '../styleguidist'),
  node_modules: path.resolve(__dirname, '../node_modules'),
  tsconfig: path.resolve(__dirname, '../tsconfig.json'),
};

module.exports = {
  context: paths.styleguidist,
  entry: './index.ts',

  devtool: 'inline-source-map',
  module: {
    rules: [
      {
        test: paths.typescript,
        include: [paths.src, paths.styleguidist],
        loader: require.resolve('babel-loader'),
      },
      {
        loader: 'file-loader',
        include: [regex.fonts],
        options: {
          name: 'fonts/[name].[ext]',
        },
      },
      {
        loader: 'file-loader',
        include: [regex.css],
        options: {
          name: 'static/[name].[hash:8].[ext]',
        },
      },
    ],
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
  },
  plugins: [
    new ForkTsCheckerWebpackPlugin({
      typescript: resolve.sync('typescript', {
        basedir: paths.node_modules,
      }),
      async: false,
      checkSyntacticErrors: true,
      tsconfig: paths.tsconfig,
      compilerOptions: {
        module: 'esnext',
        moduleResolution: 'node',
        resolveJsonModule: true,
        isolatedModules: true,
        noEmit: true,
        jsx: 'preserve',
      },
      reportFiles: ['**', '!**/*.json', '!**/__tests__/**', '!**/?(*.)(spec|test).*', '!**/src/setupProxy.*', '!**/src/setupTests.*'],
      watch: paths.src,
      silent: true,
      formatter: typescriptFormatter,
    }),

    // This is necessary to emit hot updates (currently CSS only):
    new webpack.HotModuleReplacementPlugin(),
    // Watcher doesn't work well if you mistype casing in a path so we use
    // a plugin that prints an error when you attempt to do this.
    // See https://github.com/facebook/create-react-app/issues/240
    new CaseSensitivePathsPlugin(),
    // If you require a missing module and then `npm install` it, you still have
    // to restart the development server for Webpack to discover it. This plugin
    // makes the discovery automatic so you don't have to restart.
    // See https://github.com/facebook/create-react-app/issues/186
    new WatchMissingNodeModulesPlugin(paths.appNodeModules),
  ],
};
