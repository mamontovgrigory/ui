const path = require('path');
const docGenTypescript = require('react-docgen-typescript');

module.exports = {
  pagePerSection: true,
  compilerConfig: {
    transforms: {
      modules: false,
    },
    objectAssign: 'Object.assign',
  },
  sections: [
    {
      name: 'UI Components',
      components: 'src/components/*/*.tsx',
    },
    {
      name: 'Redux Form',
      components: 'src/redux-form/*/*.tsx',
    },
  ],
  propsParser: docGenTypescript.withCustomConfig('./tsconfig.json', {
    componentNameResolver: (exp, source) => exp.getName() === 'StyledComponentClass' && docGenTypescript.getDefaultExportForFile(source),
  }).parse,
  webpackConfig: require('./config/webpack.config.js'),
  styleguideComponents: {
    Wrapper: path.join(__dirname, 'styleguidist'),
  },
};
