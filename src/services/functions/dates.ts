import moment from 'moment';
const isRussianDate = (date: any) => /^\d{2}.\d{2}.\d{4}$/.test(date);
export const normalizeDate = (date: string | Date | moment.Moment) => (isRussianDate(date) ? moment(date, 'DD.MM.YYYY') : moment(date));
