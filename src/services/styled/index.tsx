import baseStyled, { ThemedStyledInterface, ThemedStyledProps, StyledComponent as StyledComponentType } from 'styled-components';

export { css, createGlobalStyle, keyframes, ThemeProvider } from 'styled-components';

import { ITheme } from '../theme';

export type StyledComponent<C extends keyof JSX.IntrinsicElements | React.ComponentType<any>, P extends object = {}> = StyledComponentType<
  C,
  P,
  ITheme,
  never
>;

export type ThemedProps<P> = ThemedStyledProps<P, ITheme>;

export default baseStyled as ThemedStyledInterface<ITheme>;
