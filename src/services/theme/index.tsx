export interface IColors {
  white: string;
  transparent: string;
  blueLight: string;
  blueMain: string;
  blueHover: string;
  greyBg: string;
  greyDivider: string;
  mediumGrey: string;
  mainGrey: string;
  darkGrey: string;
  black: string;
  darkBlue: string;
  redError: string;
  redErrorDarker: string;
  purple: string;
  purpleDarker: string;
  greenSuccess: string;
  yellow: string;
  violet: string;
  dustyOrange: string;
  brown: string;
}

export interface IIndent {
  indent2: string;
  indent4: string;
  indent8: string;
  indent12: string;
  indent16: string;
  indent20: string;
  indent22: string;
  indent24: string;
  indent32: string;
  indent40: string;
  indent48: string;
}

export interface IFonts {
  WhitneyBook: string;
  WhitneyMedium: string;
  WhitneyLight: string;
}

export interface Size {
  small: string;
  medium: string;
}

export interface ITheme {
  colors: IColors;
  indent: IIndent;
  fonts: IFonts;
  size: Size;
}

export enum Colors {
  White = '#ffffff',
  Transparent = 'rgba(255, 255, 255, 0)',
  BlueLight = '#e5f9fd',
  BlueMain = '#00bbe4',
  BlueHover = '#00aad5',
  GreyBg = '#f7f8f9',
  GreyDivider = '#dfe0e3',
  MediumGrey = '#c9cdd4',
  MainGrey = '#9ba4b1',
  DarkGrey = '#818a97',
  Black = '#1a1b1c',
  DarkBlue = '#263746',
  RedError = '#ff5d5d',
  RedErrorDarker = '#e55252',
  Purple = '#77a1f5',
  PurpleDarker = '#5b8cee',
  GreenSuccess = '#3cbb75',
  Yellow = '#f6bb42',
  Violet = '#97a2ea',
  DustyOrange = '#f06e32',
  Brown = '#978f81',
}

const theme: ITheme = {
  colors: {
    white: Colors.White,
    transparent: Colors.Transparent,
    blueLight: Colors.BlueLight,
    blueMain: Colors.BlueMain,
    blueHover: Colors.BlueHover,
    greyBg: Colors.GreyBg,
    greyDivider: Colors.GreyDivider,
    mediumGrey: Colors.MediumGrey,
    mainGrey: Colors.MainGrey,
    darkGrey: Colors.DarkGrey,
    black: Colors.Black,
    darkBlue: Colors.DarkBlue,
    redError: Colors.RedError,
    redErrorDarker: Colors.RedErrorDarker,
    purple: Colors.Purple,
    purpleDarker: Colors.PurpleDarker,
    greenSuccess: Colors.GreenSuccess,
    yellow: Colors.Yellow,
    violet: Colors.Violet,
    dustyOrange: Colors.DustyOrange,
    brown: Colors.Brown,
  },
  indent: {
    indent2: '2px',
    indent4: '4px',
    indent8: '8px',
    indent12: '12px',
    indent16: '16px',
    indent20: '20px',
    indent22: '22px',
    indent24: '24px',
    indent32: '32px',
    indent40: '40px',
    indent48: '48px',
  },
  fonts: {
    WhitneyBook: "'Whitney Book', sans-serif",
    WhitneyMedium: "'Whitney Medium', sans-serif",
    WhitneyLight: "'Whitney Light', sans-serif",
  },
  size: {
    small: 'small',
    medium: 'medium',
  },
};

export default theme;
