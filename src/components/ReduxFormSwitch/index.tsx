import * as React from 'react';
import { Field, WrappedFieldProps } from 'redux-form';

import Switch, { Props as SwitchProps } from '../Switch';

const renderField: React.FC<WrappedFieldProps & SwitchProps> = ({ input: { value, onChange }, label, ...rest }) => (
  <Switch {...rest} checked={value} label={label} onChange={onChange} />
);

type Props = {
  name: string;
} & any;

const ReduxFormSwitch: React.FC<Props> = (props) => <Field {...props} component={renderField} />;

export default ReduxFormSwitch;
