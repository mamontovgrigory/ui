import React from 'react';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import MuiSelect from '@material-ui/core/Select';

export type Props = {
  value: string | number;
  label: React.ReactNode;
  disabled?: boolean;
  options: {
    value: string | number;
    label: string;
  }[];

  onChange?(value: string): void;
};

const Select: React.FC<Props> = ({ value, label, disabled, options, onChange }) => {
  const handleChange = onChange
    ? (event: React.ChangeEvent<HTMLSelectElement>) => {
        onChange(event.target.value);
      }
    : null;
  const defaultOption = {
    value: '',
    label: '',
  };
  return (
    <FormControl fullWidth={true} disabled={disabled}>
      <InputLabel>{label}</InputLabel>
      <MuiSelect value={value} onChange={handleChange}>
        {[defaultOption, ...options].map((option, index) => (
          <MenuItem key={index} value={option.value}>
            {option.label}
          </MenuItem>
        ))}
      </MuiSelect>
    </FormControl>
  );
};

export default Select;
