Select example

```jsx inside Markdown
initialState = {
  value: 1,
  label: 'test',
  options: [{ value: 1, label: 'test 1' }, { value: 2, label: 'test 2' }, { value: 3, label: 'test 3' }],
};

<Select value={state.value} label={state.label} options={state.options} />;
```
