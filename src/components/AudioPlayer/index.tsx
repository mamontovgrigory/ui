import * as React from 'react';
import ReactAudioPlayer from 'react-audio-player';

interface IProps {
  src: string;
}

const AudioPlayer = ({ src }: IProps) => <ReactAudioPlayer src={src} autoPlay={false} controls={true} />;

export default AudioPlayer;
