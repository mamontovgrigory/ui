Chart example

```jsx inside Markdown
import Row from '../Row';
import Col from '../Col';

initialState = {
  values: [
    {
      name: 'Monday',
      options: [
        {
          label: 'USD',
          value: 33.7,
        },
        {
          label: 'EUR',
          value: 40.41,
        },
      ],
    },
    {
      name: 'Tuesday',
      options: [
        {
          label: 'USD',
          value: 63.8,
        },
        {
          label: 'EUR',
          value: 70.51,
        },
      ],
    },
    {
      name: 'Wednesday',
      options: [
        {
          label: 'USD',
          value: 55.4,
        },
        {
          label: 'EUR',
          value: 50.25,
        },
      ],
    },
    {
      name: 'Thursday',
      options: [
        {
          label: 'USD',
          value: 68.15,
        },
        {
          label: 'EUR',
          value: 79.0,
        },
      ],
    },
    {
      name: 'Friday',
      options: [
        {
          label: 'USD',
          value: 63.65,
        },
        {
          label: 'EUR',
          value: 70.45,
        },
      ],
    },
  ],
};

<Row>
  <Col xs={12}>
    <Chart values={state.values} />
  </Col>
</Row>;
```
