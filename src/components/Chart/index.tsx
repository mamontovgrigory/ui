import React from 'react';
import { LineChart, XAxis, YAxis, CartesianGrid, Tooltip, Line, Legend } from 'recharts';

interface IProps {
  width?: number;
  height?: number;
  values: {
    name: string;
    options: {
      label: string;
      value: number;
      color?: string;
    }[];
  }[];
}

const Chart = ({ width = 730, height = 250, values }: IProps) => {
  const lines = [];
  const data = values.map(({ name, options }) => {
    const dataItem = { name };
    options.forEach(({ label, value, color }) => {
      if (!lines.some(({ key }) => key === label)) {
        lines.push({
          key: label,
          color,
        });
      }
      dataItem[label] = value;
    });
    return dataItem;
  });

  return (
    <LineChart width={width} height={height} data={data} margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
      <CartesianGrid strokeDasharray="3 3" />
      <XAxis dataKey="name" />
      <YAxis />
      <Tooltip />
      <Legend />
      {lines.map(({ key, color }) => (
        <Line type="monotone" dataKey={key} stroke={color} />
      ))}
    </LineChart>
  );
};

export default Chart;
