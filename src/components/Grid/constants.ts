export const TABLE_MESSAGES = {
  noData: 'Нет данных',
};

export const EDIT_COLUMN_MESSAGES = {
  addCommand: 'Добавить',
  editCommand: 'Редактировать',
  deleteCommand: 'Удалить',
  commitCommand: 'Подтвердить',
  cancelCommand: 'Отменить',
};

export const GROUPING_PANEL_MESSAGES = {
  groupByColumn: 'Перетащите для группировки',
};

export const FILTER_ROW_MESSAGES = {
  filterPlaceholder: '',
  contains: 'Содержит',
  notContains: 'Не содержит',
  startsWith: 'Начинается',
  endsWith: 'Кончается',
  equal: 'Равно',
  notEqual: 'Не равно',
  greaterThan: 'Больше',
  greaterThanOrEqual: 'Больше или равно',
  lessThan: 'Меньше',
  lessThanOrEqual: 'Меньше или равно',
};

export const PAGING_PANEL_MESSAGES = {
  showAll: 'Все',
  rowsPerPage: 'Строк на странице',
  info: ({ from, to, count }) => `${from} - ${to} из ${count}`,
};
