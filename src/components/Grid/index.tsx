import React from 'react';

import {
  Column,
  EditingState,
  Filter,
  FilteringState,
  GroupingState,
  IntegratedFiltering,
  IntegratedGrouping,
  IntegratedPaging,
  IntegratedSelection,
  IntegratedSorting,
  PagingState,
  RowDetailState,
  SelectionState,
  SortingState,
} from '@devexpress/dx-react-grid';
import {
  ColumnChooser,
  DragDropProvider,
  Grid as ReactGrid,
  GroupingPanel,
  PagingPanel,
  Table,
  TableEditColumn,
  TableEditRow,
  TableFilterRow,
  TableFixedColumns,
  TableGroupRow,
  TableHeaderRow,
  TableColumnReordering,
  TableColumnResizing,
  TableColumnVisibility,
  TableRowDetail,
  TableSelection,
  Toolbar,
} from '@devexpress/dx-react-grid-material-ui';

import Paper from '../Paper';
import { Command, FilterCell, FilterValuesCollector, FilterValuesProvider } from './components';
import { TABLE_MESSAGES, EDIT_COLUMN_MESSAGES, FILTER_ROW_MESSAGES, GROUPING_PANEL_MESSAGES, PAGING_PANEL_MESSAGES } from './constants';

interface IColumn extends Column {
  width?: number;
  hidden?: boolean;
}

interface IColumnWidth {
  columnName: string;
  width: number;
}

interface IProps {
  columns: IColumn[];
  data?: any[];
  showSelectionColumn?: boolean;
  keyColumn?: string;
  editCellRenderMethods?: { [key: string]: (props: TableEditRow.CellProps) => React.ReactNode };
  expandedRows?: (string | number)[];
  editColumnWidth?: number;
  selectFilterValues?: {
    [key: string]: string[];
  };
  formats?: {
    [key: string]: (value: string) => string;
  };
  filterFormats?: {
    [key: string]: (value: string) => string;
  };

  setCellStyle?(row: any, value, columnName: string): object;
  renderRowDetail?(row: any): any;
  onSelect?(rows: any[]): void;
  onAdd?(row: any): void;
  onDoubleClick?(row: any): void;
  onEdit?(row: any): void;
  onDelete?(ids: (number | string)[]): void;
  onChangeColumns?(columns: IColumn[]): void;
  onChangeExpandedRows?(ids: (number | string)[]): void;
}

const Grid = ({
  data,
  columns,
  showSelectionColumn = false,
  keyColumn = 'id',
  expandedRows,
  editColumnWidth,
  editCellRenderMethods,
  renderRowDetail,
  setCellStyle,
  formats,
  filterFormats,
  selectFilterValues,
  onSelect,
  onAdd,
  onEdit,
  onDelete,
  onDoubleClick,
  onChangeColumns,
  onChangeExpandedRows,
}: IProps) => {
  const rows = data || [];
  const pageSizes = [25, 50, 100, 0];
  const [selection, setSelection] = React.useState<number[]>([]);
  const [columnOrder, setColumnOrder] = React.useState<string[]>([]);
  const [hiddenColumnNames, setHiddenColumnNames] = React.useState<string[]>([]);
  const [columnWidths, setColumnWidths] = React.useState<IColumnWidth[]>([]);
  const [pageSize, setPageSize] = React.useState<number>(pageSizes[1]);
  const [expandedRowIds, setExpandedRowIds] = React.useState<(string | number)[]>(expandedRows || []);
  const [filters, setFilters] = React.useState<Filter[]>([]);

  const selectEnabled = Boolean(onSelect);
  const addEnabled = Boolean(onAdd);
  const editEnabled = Boolean(onEdit);
  const deleteEnabled = Boolean(onDelete);
  const addOrEditOrDeleteEnabled = addEnabled || deleteEnabled;

  React.useEffect(() => {
    setColumnOrder(columns.map(({ name }) => name));
    setHiddenColumnNames(columns.filter(({ hidden }) => hidden).map(({ name }) => name));
    setColumnWidths(
      columns.map(({ name, width }) => ({
        columnName: name,
        width: width ? width : 150,
      }))
    );
  }, [columns]);

  React.useEffect(() => {
    setExpandedRowIds(expandedRows);
  }, [expandedRows]);

  const getRowId = (row) => row[keyColumn];

  const handleSelect = (newSelection) => {
    setSelection(newSelection);
    if (newSelection && onSelect) {
      onSelect(rows.filter((item) => newSelection.includes(item[keyColumn])));
    }
  };

  const commitChanges = ({ added, changed, deleted }) => {
    if (added && addEnabled) {
      onAdd(added[0]);
    }
    if (changed && editEnabled) {
      const key = Object.keys(changed)[0];
      onEdit({ ...rows.find((item) => String(item[keyColumn]) === String(key)), ...changed[key] });
    }
    if (deleted && deleteEnabled) {
      onDelete(deleted);
    }
  };

  const TableCell: any = Table.Cell; //TODO: Fix types

  const renderEditCell = (props) => {
    const { column } = props;
    if (editCellRenderMethods && editCellRenderMethods[column.name]) {
      return <TableCell>{editCellRenderMethods[column.name](props)}</TableCell>;
    }
    return <TableEditRow.Cell {...props} />;
  };

  const renderTableRow = (rowProps) => (
    <Table.Row
      {...rowProps}
      onDoubleClick={() => {
        if (onDoubleClick) {
          onDoubleClick(rowProps.row);
        }
      }}
    />
  );

  const renderTableCell = ({ value, ...restProps }) => {
    const style = setCellStyle ? setCellStyle(restProps.row, value, restProps.column.name) : {};
    const fn = formats && formats[restProps.column.name];
    return (
      <TableCell {...restProps} style={style}>
        {fn ? fn(value) : value}
      </TableCell>
    );
  };

  const columnExtensions = columns.map((column) => ({
    columnName: column.name,
    width: column.width,
  }));

  const onOrderChange = (columnNames: string[]) => {
    setColumnOrder(columnNames);
    if (onChangeColumns) {
      onChangeColumns(columnNames.map((name) => columns.find((column) => column.name === name)));
    }
  };

  const onColumnWidthsChange = (newColumnWidths: IColumnWidth[]) => {
    setColumnWidths(newColumnWidths);
    if (onChangeColumns) {
      onChangeColumns(
        newColumnWidths.map(({ columnName, width }) => ({
          ...columns.find((column) => column.name === columnName),
          width,
        }))
      );
    }
  };

  const onHiddenColumnNamesChange = (hiddenColumnNames: string[]) => {
    setHiddenColumnNames(hiddenColumnNames);
    if (onChangeColumns) {
      onChangeColumns(
        columns.map((column) => ({
          ...column,
          hidden: hiddenColumnNames.includes(column.name),
        }))
      );
    }
  };

  const onExpandedRowIdsChange = (ids) => {
    setExpandedRowIds(ids);
    if (onChangeExpandedRows) {
      onChangeExpandedRows(ids);
    }
  };

  const changeFilters = (newFilters: Filter[]) => setFilters(newFilters.filter((f) => f.value));

  const tableFilterRow = React.useMemo(
    () => (
      <TableFilterRow
        showFilterSelector={true}
        cellComponent={(cellProps) => <FilterCell {...cellProps} selectFilterValues={selectFilterValues} filterFormats={filterFormats} />}
        messages={FILTER_ROW_MESSAGES}
      />
    ),
    []
  );

  return (
    <Paper>
      <ReactGrid rows={rows} columns={columns} getRowId={getRowId}>
        <FilteringState filters={filters} onFiltersChange={changeFilters} />
        <SortingState />

        {selectEnabled && <SelectionState onSelectionChange={handleSelect} selection={selection} />}

        <GroupingState />
        <PagingState pageSize={pageSize} onPageSizeChange={setPageSize} />

        <FilterValuesCollector selectFilterValues={selectFilterValues} />
        <IntegratedGrouping />
        <IntegratedFiltering />
        <IntegratedSorting />
        <IntegratedPaging />
        {selectEnabled && <IntegratedSelection />}

        {addOrEditOrDeleteEnabled && (
          <EditingState onCommitChanges={commitChanges} columnExtensions={[{ columnName: keyColumn, editingEnabled: false }]} />
        )}
        {renderRowDetail && <RowDetailState expandedRowIds={expandedRowIds} onExpandedRowIdsChange={onExpandedRowIdsChange} />}
        <DragDropProvider />
        <Table
          messages={TABLE_MESSAGES}
          columnExtensions={columnExtensions}
          rowComponent={renderTableRow}
          cellComponent={renderTableCell}
        />
        <TableColumnReordering order={columnOrder} onOrderChange={onOrderChange} />
        <TableColumnResizing columnWidths={columnWidths} onColumnWidthsChange={onColumnWidthsChange} />
        {selectEnabled && (
          <TableSelection highlightRow={true} showSelectAll={showSelectionColumn} showSelectionColumn={showSelectionColumn} />
        )}

        <TableHeaderRow showSortingControls={true} />
        {renderRowDetail && <TableRowDetail contentComponent={renderRowDetail} />}
        <TableColumnVisibility hiddenColumnNames={hiddenColumnNames} onHiddenColumnNamesChange={onHiddenColumnNamesChange} />
        {addOrEditOrDeleteEnabled && <TableEditRow cellComponent={renderEditCell} />}
        {addOrEditOrDeleteEnabled && (
          <TableEditColumn
            showAddCommand={addEnabled}
            showEditCommand={editEnabled}
            showDeleteCommand={deleteEnabled}
            commandComponent={Command}
            messages={EDIT_COLUMN_MESSAGES}
            width={editColumnWidth}
          />
        )}
        {tableFilterRow}
        <PagingPanel pageSizes={pageSizes} messages={PAGING_PANEL_MESSAGES} />

        <TableGroupRow />
        {addOrEditOrDeleteEnabled && <TableFixedColumns leftColumns={[TableEditColumn.COLUMN_TYPE]} />}
        <Toolbar />
        <ColumnChooser />
        <GroupingPanel showSortingControls={true} messages={GROUPING_PANEL_MESSAGES} />
        <FilterValuesProvider />
      </ReactGrid>
    </Paper>
  );
};

export default Grid;
