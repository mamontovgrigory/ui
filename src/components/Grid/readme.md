Grid example

```jsx inside Markdown
initialState = {
  data: [
    {
      id: 1,
      product: 'EnviroCare Max',
      region: 'Africa',
      amount: 9516,
      saleDate: '2016-02-01',
      customer: 'Global Services',
      phone: '79102268699',
    },
    {
      id: 2,
      product: 'EnviroCare Max',
      region: 'South America',
      amount: 8231,
      saleDate: '2016-02-01',
      customer: 'Building M Inc',
      phone: '79518764922',
    },
    {
      id: 3,
      product: 'EnviroCare Max',
      region: 'North America',
      amount: 3487,
      saleDate: '2016-02-02',
      customer: 'Mercury Solar',
      phone: '79507151760',
    },
    {
      id: 4,
      product: 'SolarOne',
      region: 'Asia',
      amount: 1625,
      saleDate: '2016-02-04',
      customer: 'Mercury Solar',
      phone: '79304160008',
    },
  ],
  columns: JSON.parse(localStorage.getItem('grid')) || [
    { name: 'id', title: 'ID' },
    { name: 'product', title: 'Product' },
    { name: 'region', title: 'Region' },
    { name: 'amount', title: 'Sale Amount' },
    { name: 'saleDate', title: 'Sale Date' },
    { name: 'customer', title: 'Customer' },
    { name: 'phone', title: 'Phone' },
  ],
  selectFilterValues: {
    product: [],
    region: ['Africa', 'America'],
  },
  formats: {
    phone: (s) => s.replace(/\D/g, '').replace(/(\d{1})(\d{3})(\d{3})(\d{2})(\d{2})/, '+$1 ($2) $3-$4-$5'),
  },
};

<Grid
  columns={state.columns}
  data={state.data}
  selectFilterValues={state.selectFilterValues}
  formats={state.formats}
  showSelectionColumn={true}
  onChangeColumns={(columns) => {
    console.log('columns', columns);
    localStorage.setItem('grid', JSON.stringify(columns));
  }}
  onSelect={(selected) => console.log('selected', selected)}
/>;
```
