import React from 'react';

import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Input from '@material-ui/core/Input';
import { TableFilterRow } from '@devexpress/dx-react-grid-material-ui';
import { TableFilterRow as TableFilterRowBase } from '@devexpress/dx-react-grid';

type Props = {
  columnFilterValues?: {
    [key: string]: string[];
  };
  selectFilterValues?: {
    [key: string]: string[];
  };
  filterFormats?: {
    [key: string]: (value: string) => string;
  };
} & TableFilterRowBase.CellProps;

export const FilterCell: React.FC<Props> = ({
  columnFilterValues,
  selectFilterValues,
  filterFormats,
  column,
  children,
  filter,
  onFilter,
  ...restProps
}) => {
  const fixedValues = selectFilterValues && selectFilterValues[column.name];
  if (!fixedValues) {
    const fn = filterFormats && filterFormats[column.name];
    const onChange = (e) => {
      onFilter({ ...filter, value: fn ? fn(e.target.value) : e.target.value });
    };
    return (
      <TableFilterRow.Cell {...restProps} column={column} onFilter={onFilter} filter={filter}>
        {fn ? <Input value={filter && filter.value} onChange={onChange} /> : children}
      </TableFilterRow.Cell>
    );
  }
  const values = (fixedValues.length ? selectFilterValues[column.name] : columnFilterValues[column.name]).filter(Boolean).sort();
  const filterValues = ['', ...values];
  const filterValue = filter && filter.value;
  const onChange = (e) => {
    onFilter({ columnName: column.name, value: e.target.value, operation: 'equal' });
  };
  return (
    <TableFilterRow.Cell {...restProps} column={column} onFilter={onFilter} filter={filter}>
      <Select value={filterValue} onChange={onChange}>
        {filterValues.map((value) => (
          <MenuItem value={value || null}>{value}</MenuItem>
        ))}
      </Select>
    </TableFilterRow.Cell>
  );
};
