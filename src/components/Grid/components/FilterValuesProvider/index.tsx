import React from 'react';

import { Plugin, Template, TemplateConnector, TemplatePlaceholder } from '@devexpress/dx-react-core';
import { TableFilterRow, Table } from '@devexpress/dx-react-grid';

const isFilterTableCell = ({ tableRow, tableColumn }) =>
  tableRow.type === TableFilterRow.ROW_TYPE && tableColumn.type === Table.COLUMN_TYPE;

export const FilterValuesProvider = React.memo(() => (
  <Plugin name="FilterValuesProvider">
    <Template name="tableCell" predicate={isFilterTableCell}>
      {(params) => (
        <TemplateConnector>
          {({ columnFilterValues, selectFilterValues }) => (
            <TemplatePlaceholder params={{ ...params, columnFilterValues, selectFilterValues }} />
          )}
        </TemplateConnector>
      )}
    </Template>
  </Plugin>
));
