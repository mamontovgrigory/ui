import React from 'react';

import { Plugin, Getter } from '@devexpress/dx-react-core';

const filterValuesComputed = ({ rows, columns, getCellValue }) =>
  columns.reduce((acc, { name: colName }) => {
    const values = new Set(rows.map((r) => getCellValue(r, colName)));
    acc[colName] = Array.from(values);
    return acc;
  }, {});

type Props = {
  selectFilterValues?: {
    [key: string]: string[];
  };
};

export const FilterValuesCollector: React.FC<Props> = React.memo(({ selectFilterValues }) => (
  <Plugin name="filterValuesCollector">
    <Getter name="columnFilterValues" computed={filterValuesComputed} />
  </Plugin>
));
