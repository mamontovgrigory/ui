import * as React from 'react';

import Modal from '../Modal';
import Row from '../Row';
import Col from '../Col';
import Button from '../Button';

type Props = {
  show: boolean;

  onConfirm(): void;
  onClose(): void;
};

export const Confirm: React.FC<Props> = ({ show, children, onConfirm, onClose }) => {
  const onConfirmClick = () => {
    onClose();
    onConfirm();
  };
  return (
    <Modal show={show} onClose={onClose}>
      <div>
        <Row>
          <Col xs={12}>{children}</Col>
        </Row>
        <Row>
          <Col>
            <Button onClick={onConfirmClick}>Ок</Button>
          </Col>
          <Col>
            <Button onClick={onClose}>Отмена</Button>
          </Col>
        </Row>
      </div>
    </Modal>
  );
};

export default Confirm;
