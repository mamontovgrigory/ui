Confirm example

```jsx inside Markdown
import Button from '../Button';
initialState = { open: false };

toggle = (e) => {
  setState({ open: !state.open });
};

<>
  <Button onClick={toggle}>Open</Button>
  <Confirm show={state.open} onClose={toggle} onConfirm={() => alert('LOL')}>
    Ok?
  </Confirm>
</>;
```
