import React from 'react';

import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import { theme } from '../ThemeProvider';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    table: {
      width: '100%',
      display: 'table',
      borderCollapse: 'collapse',
      borderSpacing: 0,
    },
    row: {
      borderBottom: '1px solid rgba(0, 0, 0, 0.12)',
      '&:nth-child(even)': {
        backgroundColor: '#f5f5f5', // TODO: Get from theme
      },
    },
    header: {
      whiteSpace: 'nowrap',
      textAlign: 'left',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
    },
  })
);

type Props = {
  headers: React.ReactNode[];
  rows: React.ReactNode[][];
};

const Table: React.FC<Props> = ({ headers, rows }) => {
  const classes = useStyles(theme);
  return (
    <table className={classes.table}>
      <tr className={classes.row}>
        {headers.map((header) => (
          <th className={classes.header}>{header}</th>
        ))}
      </tr>
      {rows.map((row) => (
        <tr className={classes.row}>
          {row.map((cell) => (
            <td>{cell}</td>
          ))}
        </tr>
      ))}
    </table>
  );
};

export default Table;
