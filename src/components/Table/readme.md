Table example

```jsx inside Markdown
initialState = {
  headers: ['Марка', 'Модель', 'Количество', 'Сумма'],
  rows: [
    ['MB', 'Другое', 9, 0],
    ['MB', 'GLC', 1, 6200],
    ['MB', 'GLE', 1, 7900],
    ['MB', 'E', 4, 21800],
    ['MB', 'CLA', 1, 5500],
    ['MB', 'GLE AMG', 1, 7900],
    ['MB', 'GLA', 1, 5500],
    ['MB', 'AMG GT', 1, 3200],
  ],
};

<Table headers={state.headers} rows={state.rows} />;
```
