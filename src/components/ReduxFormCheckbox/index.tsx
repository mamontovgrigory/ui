import * as React from 'react';
import { Field, WrappedFieldProps } from 'redux-form';

import Checkbox, { Props as Checkboxrops } from '../Checkbox';

const renderField: React.FC<WrappedFieldProps & Checkboxrops> = ({ input: { value, onChange }, label, ...rest }) => (
  <Checkbox {...rest} checked={value} label={label} onChange={onChange} />
);

type Props = {
  name: string;
} & any;

const ReduxFormCheckbox: React.FC<Props> = (props) => <Field {...props} component={renderField} />;

export default ReduxFormCheckbox;
