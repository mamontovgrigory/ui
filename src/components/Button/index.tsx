import React from 'react';
import MuiButton from '@material-ui/core/Button';

type Props = {
  submit?: boolean;

  onClick?(): void;
};

const Button: React.FC<Props> = ({ submit, children, onClick }) => {
  return (
    <MuiButton type={submit ? 'submit' : 'button'} variant="contained" onClick={onClick}>
      {children}
    </MuiButton>
  );
};

export default Button;
