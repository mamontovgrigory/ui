ExpansionPanel example

```jsx inside Markdown
import Row from '../Row';
import Col from '../Col';
initialState = {
  title: 'Expansion Panel',
};

<Row>
  <Col>
    <ExpansionPanel title={state.title}>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex, sit amet blandit leo lobortis eget.
    </ExpansionPanel>
  </Col>
</Row>;
```
