import React from 'react';
import MuiExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

interface IProps {
  title: React.ReactNode;
  children: React.ReactNode;
}

const ExpansionPanel = ({ title, children }: IProps) => (
  <MuiExpansionPanel>
    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
      <Typography>{title}</Typography>
    </ExpansionPanelSummary>
    <ExpansionPanelDetails>
      <Typography>{children}</Typography>
    </ExpansionPanelDetails>
  </MuiExpansionPanel>
);

export default ExpansionPanel;
