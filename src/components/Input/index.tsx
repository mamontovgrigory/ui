import React from 'react';
import MaskedInput, { maskArray } from 'react-text-mask';
import MuiInput from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import InputAdornment from '@material-ui/core/InputAdornment';
import AccountCircle from '@material-ui/icons/AccountCircle';
import VpnKey from '@material-ui/icons/VpnKey';

export type Props = {
  value?: string | number;
  label: React.ReactNode;
  type?: 'text' | 'number' | 'password';
  adornment?: 'login' | 'password';
  disabled?: boolean;
  mask?: maskArray | ((value: string) => maskArray);

  onChange?(value: string): void;
};

const Input: React.FC<Props> = ({ value, label, type, adornment, disabled, mask, onChange }) => {
  const handleChange = onChange
    ? (event: React.ChangeEvent<HTMLInputElement>) => {
        onChange(event.target.value);
      }
    : null;
  const startAdornment = adornment && (
    <InputAdornment position="start">
      {adornment === 'login' && <AccountCircle />}
      {adornment === 'password' && <VpnKey />}
    </InputAdornment>
  );
  if (mask) {
    const renderTextMaskCustom = ({ inputRef, ...other }) => (
      <MaskedInput
        {...other}
        ref={(ref) => {
          inputRef(ref ? ref.inputElement : null);
        }}
        mask={mask}
        placeholderChar={'\u2000'}
        showMask={false}
      />
    );

    return (
      <FormControl>
        <InputLabel>{label}</InputLabel>
        <MuiInput value={value} onChange={handleChange} inputComponent={renderTextMaskCustom} />
      </FormControl>
    );
  }
  return (
    <TextField
      fullWidth
      value={value}
      label={label}
      disabled={disabled}
      type={type ? type : 'text'}
      InputProps={{
        startAdornment,
      }}
      onChange={handleChange}
    />
  );
};

export default Input;
