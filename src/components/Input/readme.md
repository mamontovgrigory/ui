Input example

```jsx inside Markdown
import ThemeProvider from '../ThemeProvider';
import Container from '../Container';
import Paper from '../Paper';
import Row from '../Row';
import Col from '../Col';
initialState = {
  label: 'Phone',
  value: '',
  mask: ['+', '7', '(', /\d/, /\d/, /\d/, ')', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/],
};

onChange = (value) => {
  setState({ value });
};

<ThemeProvider>
  <Paper>
    <Container>
      <Row>
        <Col xs={4}>
          <Input value={state.value} label={state.label} mask={state.mask} onChange={onChange} />
        </Col>
        <Col xs={4}>
          <Input label="Login" adornment="login" />
        </Col>
        <Col xs={4}>
          <Input label="Password" adornment="password" type="password" />
        </Col>
      </Row>
    </Container>
  </Paper>
</ThemeProvider>;
```
