import React from 'react';
import { Moment } from 'moment';
import { MuiPickersUtilsProvider, DatePicker as MuiDatePicker } from '@material-ui/pickers';
import DateUtils from '@date-io/moment';

interface IProps {
  value?: Moment;
  label: string | JSX.Element;

  onChange?(value: Moment): void;
}

const DatePicker = (props: IProps) => {
  const { value, label, onChange } = props;
  const locale = 'ru'; // TODO: Make locale selectable

  const handleChange = (newValue: Moment) => {
    if (onChange) {
      onChange(newValue);
    }
  };

  return (
    <MuiPickersUtilsProvider utils={DateUtils} locale={locale}>
      <MuiDatePicker label={label} format="DD.MM.YYYY" value={value ? value : null} onChange={handleChange} />
    </MuiPickersUtilsProvider>
  );
};

export default DatePicker;
