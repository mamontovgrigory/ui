DatePicker example

```jsx inside Markdown
import moment from 'moment';
import ThemeProvider from '../ThemeProvider';
initialState = {
  value: moment('03.09.2019', 'DD.MM.YYYY'),
  label: 'Date Picker',
};

<>
  <ThemeProvider>
    <DatePicker value={state.value} label={state.label} />
  </ThemeProvider>
</>;
```
