import React from 'react';
import Typography from '@material-ui/core/Typography';
import MaterialBreadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';

interface IProps {
  links?: React.ReactNode[];
  title: React.ReactNode;
}

const Breadcrumbs = ({ links, title }: IProps) => {
  const handleClick = (event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
    event.preventDefault();
  };

  return (
    <MaterialBreadcrumbs aria-label="breadcrumb">
      {links &&
        links.map((link) => (
          <Link color="inherit" onClick={handleClick}>
            {link}
          </Link>
        ))}
      <Typography color="textPrimary">{title}</Typography>
    </MaterialBreadcrumbs>
  );
};

export default Breadcrumbs;
