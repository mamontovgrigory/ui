import React from 'react';
import MuiContainer from '@material-ui/core/Container';

interface IProps {
  children: string | JSX.Element | JSX.Element[];
}

const Container = ({ children }: IProps) => <MuiContainer fixed>{children}</MuiContainer>;

export default Container;
