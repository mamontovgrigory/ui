import * as React from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import MaterialModal from '@material-ui/core/Modal';
import { theme } from '../ThemeProvider';

const getModalStyle = () => {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      position: 'absolute',
      backgroundColor: theme.palette.background.paper,
      boxShadow: theme.shadows[5],
      padding: theme.spacing(4),
      outline: 'none',
    },
  })
);

interface IProps {
  show: boolean;
  children: JSX.Element | JSX.Element[];
  onClose?(): void;
}

const Modal = (props: IProps) => {
  const { show, children, onClose } = props;
  const [modalStyle] = React.useState(getModalStyle);

  const classes = useStyles(theme);
  return (
    <MaterialModal aria-labelledby="simple-modal-title" aria-describedby="simple-modal-description" open={show} onClose={onClose}>
      <div style={modalStyle} className={classes.paper}>
        {children}
      </div>
    </MaterialModal>
  );
};

export default Modal;
