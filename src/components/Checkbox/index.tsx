import React from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import MuiCheckbox from '@material-ui/core/Checkbox';

export type Props = {
  checked?: boolean;
  value?: string | number;
  label: React.ReactNode;
  disabled?: boolean;

  onChange?(checked: boolean): void;
};

const Checkbox: React.FC<Props> = ({ value, checked, label, disabled, onChange }) => {
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (onChange) {
      onChange(event.target.checked);
    }
  };

  return (
    <FormControlLabel
      control={<MuiCheckbox color="primary" checked={checked} onChange={handleChange} value={value} disabled={disabled} />}
      label={label}
    />
  );
};

export default Checkbox;
