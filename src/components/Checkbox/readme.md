Checkbox example

```jsx inside Markdown
import ThemeProvider from '../ThemeProvider';
import Container from '../Container';
import Paper from '../Paper';
import Row from '../Row';
import Col from '../Col';
initialState = {
  label: 'Checkbox',
};

<ThemeProvider>
  <Paper>
    <Container>
      <Row>
        <Col xs={4}>
          <Checkbox label={state.label} />
        </Col>
        <Col xs={4}>
          <Checkbox label="Disabled" disabled={true} />
        </Col>
      </Row>
    </Container>
  </Paper>
</ThemeProvider>;
```
