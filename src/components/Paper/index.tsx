import React from 'react';
import MuiPaper from '@material-ui/core/Paper';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import ThemeProvider, { theme } from '../ThemeProvider';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      padding: theme.spacing(1),
    },
  })
);

interface IProps {
  children: string | JSX.Element | JSX.Element[];
}

const Paper = ({ children }: IProps) => {
  const classes = useStyles(theme);
  return (
    <ThemeProvider>
      <MuiPaper className={classes.paper}>{children}</MuiPaper>
    </ThemeProvider>
  );
};

export default Paper;
