Menu example

```jsx inside Markdown
import Button from '../Button';
initialState = {
  options: [
    {
      label: 'test 1',
    },
    {
      label: 'test 2',
    },
  ],
};

<>
  <Menu options={state.options}>
    <Button>Open</Button>
  </Menu>
</>;
```
