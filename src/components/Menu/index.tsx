import React from 'react';

import MenuItem from '@material-ui/core/MenuItem';
import MuiMenu from '@material-ui/core/Menu';

export interface IOption {
  label: string | Element;
  onClick?(): void;
}

interface IProps {
  options: IOption[];
  children: React.ReactNode;
}

const Menu = (props: IProps) => {
  const { children, options } = props;
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);

  const openMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const closeMenu = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <span onClick={openMenu} color="inherit">
        {children}
      </span>
      <MuiMenu
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        keepMounted
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        open={open}
        onClose={closeMenu}
      >
        {options.map((option, index) => {
          const onClick = () => {
            closeMenu();
            if (option.onClick) {
              option.onClick();
            }
          };
          return (
            <MenuItem key={index} onClick={onClick}>
              {option.label}
            </MenuItem>
          );
        })}
      </MuiMenu>
    </>
  );
};

export default Menu;
