import React from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import MuiSwitch from '@material-ui/core/Switch';

export type Props = {
  checked?: boolean;
  label: React.ReactNode;
  disabled?: boolean;

  onChange?(checked: boolean): void;
};

const Switch: React.FC<Props> = ({ checked, label, disabled, onChange }) => {
  const handleChange = onChange
    ? (event: React.ChangeEvent<HTMLInputElement>, checked: boolean) => {
        onChange(checked);
      }
    : null;

  return (
    <FormControlLabel control={<MuiSwitch checked={checked} onChange={handleChange} color="primary" />} label={label} disabled={disabled} />
  );
};

export default Switch;
