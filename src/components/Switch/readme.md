Input example

```jsx inside Markdown
import ThemeProvider from '../ThemeProvider';
import Container from '../Container';
import Paper from '../Paper';
import Row from '../Row';
import Col from '../Col';
initialState = {
  label: 'Switch',
  checked: true,
};

onChange = (checked) => {
  setState({ checked });
};

<ThemeProvider>
  <Paper>
    <Container>
      <Row>
        <Col xs={12}>
          <Switch checked={state.checked} label={state.label} onChange={onChange} />
        </Col>
      </Row>
    </Container>
  </Paper>
</ThemeProvider>;
```
