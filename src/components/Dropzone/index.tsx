import React from 'react';

import ReactDropzone from 'react-dropzone';
import styled from 'styled-components';

const getColor = (props) => {
  if (props.isDragAccept) {
    return '#00e676';
  }
  if (props.isDragReject) {
    return '#ff1744';
  }
  if (props.isFocused) {
    return '#2196f3';
  }
  return '#eeeeee';
};

const Container = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 20px;
  border-width: 2px;
  border-radius: 2px;
  border-color: ${(props) => getColor(props)};
  border-style: dashed;
  background-color: #fafafa;
  color: #bdbdbd;
  outline: none;
  transition: border 0.24s ease-in-out;
`;

type Props = {
  maxFiles?: number;
  onChange?(files: File[]): void;
};

const Dropzone: React.FC<Props> = (props) => {
  const [files, setFiles] = React.useState<File[]>();

  const onDrop = (acceptedFiles) => {
    setFiles(acceptedFiles);
    if (props.onChange) {
      props.onChange(acceptedFiles);
    }
  };

  return (
    <ReactDropzone maxFiles={props.maxFiles} onDrop={onDrop}>
      {({ getRootProps, getInputProps }) => (
        <section>
          <Container {...getRootProps()}>
            <input {...getInputProps()} />
            <p>{props.children}</p>
          </Container>
          {files && (
            <aside>
              {files.map((file) => (
                <div key={file.name}>
                  {file.name} - {file.size} bytes
                </div>
              ))}
            </aside>
          )}
        </section>
      )}
    </ReactDropzone>
  );
};

export default Dropzone;
