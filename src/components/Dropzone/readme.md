ExpansionPanel example

```jsx inside Markdown
import Row from '../Row';
import Col from '../Col';

initialState = {
  maxFiles: 1,
  onChange: (files) => console.log(files),
};
<Row>
  <Col>
    <Dropzone maxFiles={1} text={state.text} onChange={onChange}>
      Drag 'n' drop some files here, or click to select files
    </Dropzone>
  </Col>
</Row>;
```
