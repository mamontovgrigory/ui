import * as React from 'react';
import Grid from '@material-ui/core/Grid';

interface IProps {
  children: React.ReactNode;
  direction?: 'row' | 'row-reverse' | 'column' | 'column-reverse';
  justify?: 'flex-start' | 'center' | 'flex-end' | 'space-between' | 'space-around' | 'space-evenly';
  alignItems?: 'flex-start' | 'center' | 'flex-end' | 'stretch' | 'baseline';
  className?: string;
  style?: React.CSSProperties;
}

const Row = ({ children, ...props }: IProps) => (
  <Grid container spacing={3} {...props}>
    {children}
  </Grid>
);

export default Row;
