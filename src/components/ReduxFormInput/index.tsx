import * as React from 'react';
import { Field, WrappedFieldProps } from 'redux-form';

import Input, { Props as InputProps } from '../Input';

const renderField: React.FC<WrappedFieldProps & InputProps> = ({ input: { value, onChange }, label, ...rest }) => (
  <Input {...rest} value={value} label={label} onChange={onChange} />
);

type Props = {
  name: string;
} & any;

const ReduxFormInput: React.FC<Props> = (props) => <Field {...props} component={renderField} />;

export default ReduxFormInput;
