import * as React from 'react';
import Grid, { GridSize } from '@material-ui/core/Grid';

interface IProps {
  xs?: GridSize;
  sm?: GridSize;
  md?: GridSize;
  lg?: GridSize;
  xl?: GridSize;
  className?: string;
  style?: React.CSSProperties;
  children: React.ReactNode;
}

const Row = ({ children, ...props }: IProps) => (
  <Grid item {...props}>
    {children}
  </Grid>
);

export default Row;
