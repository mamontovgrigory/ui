Col example

```jsx inside Markdown
import Row from '../Row';
import Paper from '../Paper';
initialState = {};

<Row>
  <Col xs={12}>
    <Paper>xs=12</Paper>
  </Col>
  <Col xs={6}>
    <Paper>xs=6</Paper>
  </Col>
  <Col xs={6}>
    <Paper>xs=6</Paper>
  </Col>
  <Col xs={3}>
    <Paper>xs=3</Paper>
  </Col>
  <Col xs={3}>
    <Paper>xs=3</Paper>
  </Col>
  <Col xs={3}>
    <Paper>xs=3</Paper>
  </Col>
  <Col xs={3}>
    <Paper>xs=3</Paper>
  </Col>
</Row>;
```
