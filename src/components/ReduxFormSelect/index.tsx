import * as React from 'react';
import { Field, WrappedFieldProps } from 'redux-form';

import Select, { Props as SelectProps } from '../Select';

const renderField: React.FC<WrappedFieldProps & SelectProps> = ({ input: { value, onChange }, label, ...rest }) => (
  <Select {...rest} value={value} label={label} onChange={onChange} />
);

type Props = {
  name: string;
} & any;

const ReduxFormInput: React.FC<Props> = (props) => <Field {...props} component={renderField} />;

export default ReduxFormInput;
