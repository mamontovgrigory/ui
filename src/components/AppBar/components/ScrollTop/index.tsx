import React from 'react';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Zoom from '@material-ui/core/Zoom';

import { theme } from '../../../ThemeProvider';
import Fab from '@material-ui/core/Fab';

type Props = {
  id: string;
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      position: 'fixed',
      bottom: theme.spacing(2),
      right: theme.spacing(2),
    },
  })
);

export const ScrollTop: React.FC<Props> = ({ id }) => {
  const classes = useStyles(theme);
  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 100,
  });

  const handleClick = (event: React.MouseEvent<HTMLDivElement>) => {
    const anchor = ((event.target as HTMLDivElement).ownerDocument || document).querySelector(`#${id}`);

    if (anchor) {
      anchor.scrollIntoView({ behavior: 'smooth', block: 'center' });
    }
  };

  return (
    <Zoom in={trigger}>
      <div onClick={handleClick} role="presentation" className={classes.root}>
        <Fab color="primary" size="medium">
          <KeyboardArrowUpIcon />
        </Fab>
      </div>
    </Zoom>
  );
};
