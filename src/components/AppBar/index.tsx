import React from 'react';
import MuiAppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import ExitToApp from '@material-ui/icons/ExitToApp';
import Container from '../Container';
import { theme } from '../ThemeProvider';
import Menu, { IOption } from '../Menu';
import { ScrollTop } from './components/ScrollTop';

type Props = {
  title: string | JSX.Element;
  options?: IOption[];
  fixed?: boolean;
  scrollTop?: boolean;
  account?: {
    name: string;
    logout?(): void;
  };
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    menuIcon: {
      marginRight: theme.spacing(2),
    },
    iconButton: {
      color: '#fff',
    },
    title: {
      flexGrow: 1,
    },
  })
);

const AppBar: React.FC<Props> = ({ title, options, account, fixed, scrollTop }) => {
  const classes = useStyles(theme);
  const id = 'back-to-top-anchor';
  return (
    <React.Fragment>
      <CssBaseline />
      <MuiAppBar position={fixed ? 'fixed' : 'static'}>
        <Container>
          <Toolbar>
            {options && options.length > 0 && (
              <Menu options={options}>
                <IconButton className={classes.iconButton}>
                  <MenuIcon />
                </IconButton>
              </Menu>
            )}
            <Typography variant="h6" className={classes.title}>
              {title}
            </Typography>
            {account && (
              <>
                <AccountCircle className={classes.menuIcon} />
                {account.name}
                <IconButton onClick={account.logout} className={classes.iconButton}>
                  <ExitToApp />
                </IconButton>
              </>
            )}
          </Toolbar>
        </Container>
      </MuiAppBar>
      <Toolbar id={id} />
      {scrollTop && <ScrollTop id={id} />}
    </React.Fragment>
  );
};

export default AppBar;
