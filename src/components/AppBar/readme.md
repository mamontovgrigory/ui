AppBar example

```jsx inside Markdown
import ThemeProvider from '../ThemeProvider';
initialState = {
  title: 'App Bar',
  options: [{ label: 'Users' }, { label: 'Groups' }],
  account: {
    name: 'User',
  },
};

<>
  <ThemeProvider>
    <AppBar title={state.title} options={state.options} account={state.account} />
  </ThemeProvider>
</>;
```
