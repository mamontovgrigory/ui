import React from 'react';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import teal from '@material-ui/core/colors/teal';

export const theme = createMuiTheme({
  palette: {
    type: 'light',
    primary: {
      main: teal[400], // TODO: Make editable color
    },
    secondary: {
      main: teal[400], // TODO: Make editable color
    },
  },
});

interface IProps {
  children: JSX.Element | JSX.Element[];
}

const ThemeProvider = ({ children }: IProps) => <MuiThemeProvider theme={theme}>{children}</MuiThemeProvider>;

export default ThemeProvider;
