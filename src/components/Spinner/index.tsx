import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';

const Spinner: React.FC = () => <CircularProgress color="primary" />;

export default Spinner;
