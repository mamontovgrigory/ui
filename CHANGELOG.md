## [0.8.1](https://bitbucket.org/mamontovgrigory/ui/compare/v0.8.0...v0.8.1) (2022-02-09)


### Bug Fixes

* dropzone export ([ba166fa](https://bitbucket.org/mamontovgrigory/ui/commits/ba166fa2e04e80e220ae7f544db3ac6fc35b9bd4))

# [0.8.0](https://bitbucket.org/mamontovgrigory/ui/compare/v0.7.1...v0.8.0) (2022-02-09)


### Features

* dropzone ([16e476e](https://bitbucket.org/mamontovgrigory/ui/commits/16e476eddf3505741726b253092dab97fb578caf))

## [0.7.1](https://bitbucket.org/mamontovgrigory/ui/compare/v0.7.0...v0.7.1) (2022-02-05)


### Bug Fixes

* menu children wrapper ([b358187](https://bitbucket.org/mamontovgrigory/ui/commits/b358187b1206b5cb934a8dcb88695d58f12705db))

# [0.7.0](https://bitbucket.org/mamontovgrigory/ui/compare/v0.6.0...v0.7.0) (2021-12-11)


### Features

* table ([815e0c9](https://bitbucket.org/mamontovgrigory/ui/commits/815e0c955513cd8b0bde08125988d6a0c648c0cf))

# [0.6.0](https://bitbucket.org/mamontovgrigory/ui/compare/v0.5.1...v0.6.0) (2021-11-30)


### Bug Fixes

* grid input filters ([7e00140](https://bitbucket.org/mamontovgrigory/ui/commits/7e00140d64ec1fcef03117c8694781dcf4654b1b))


### Features

* test ([cb5299d](https://bitbucket.org/mamontovgrigory/ui/commits/cb5299db8860953d2db3ec071d92a07a7188c69e))

## [0.5.1](https://bitbucket.org/mamontovgrigory/ui/compare/v0.5.0...v0.5.1) (2021-11-14)


### Bug Fixes

* grid select filter empty values ([5081d86](https://bitbucket.org/mamontovgrigory/ui/commits/5081d8634ffc123cbf2033719759250347cfc099))

# [0.5.0](https://bitbucket.org/mamontovgrigory/ui/compare/v0.4.0...v0.5.0) (2021-11-14)


### Features

* grid select filter ([eb48975](https://bitbucket.org/mamontovgrigory/ui/commits/eb4897520cfdc6e4274172ce210bead53cb25386))

# [0.4.0](https://bitbucket.org/mamontovgrigory/ui/compare/v0.3.1...v0.4.0) (2020-12-05)


### Features

* spinner ([7b19896](https://bitbucket.org/mamontovgrigory/ui/commits/7b1989607de817fc24c97734c1bdcedc343564d4))

## [0.3.1](https://bitbucket.org/mamontovgrigory/ui/compare/v0.3.0...v0.3.1) (2020-11-08)


### Bug Fixes

* test ([cfac2ea](https://bitbucket.org/mamontovgrigory/ui/commits/cfac2eae229af188e620dda696d6f7f2fc0a15ad))

# [0.3.0](https://bitbucket.org/mamontovgrigory/ui/compare/v0.2.0...v0.3.0) (2020-11-08)


### Bug Fixes

* local semantic release ([89fd0f4](https://bitbucket.org/mamontovgrigory/ui/commits/89fd0f4345948059ea5681586ed2a7128c40a7d7))
* semantic release dev dependency ([2f09f83](https://bitbucket.org/mamontovgrigory/ui/commits/2f09f83e740f886518d7f6b80593e0699e36e09d))


### Features

* redux form checkbox ([e6127e7](https://bitbucket.org/mamontovgrigory/ui/commits/e6127e7b323908e913c6dd4e268ff0be1320a430))

# [0.2.0](https://bitbucket.org/mamontovgrigory/ui/compare/v0.1.1...v0.2.0) (2020-11-01)


### Features

* **grid:** select by row click disabled ([337285f](https://bitbucket.org/mamontovgrigory/ui/commits/337285f8a1375e18432c67daea006a3de5ef2593))

## [0.1.1](https://bitbucket.org/mamontovgrigory/ui/compare/v0.1.0...v0.1.1) (2020-10-13)


### Bug Fixes

* standard-version removed ([239df64](https://bitbucket.org/mamontovgrigory/ui/commits/239df640836d0751db5c0c848ced305df977393d))

# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.10](https://bitbucket.org/mamontovgrigory/ui/compare/v0.0.9...v0.0.10) (2020-10-13)

### Reverts

- revert release script ([ecdf045](https://bitbucket.org/mamontovgrigory/ui/commit/ecdf04592f8f6aa28c703c67d4527afc29a4cc82))

### [0.0.9](https://bitbucket.org/mamontovgrigory/ui/compare/v0.0.8...v0.0.9) (2020-10-13)

### Others

- release script without yarn publish ([7383ce0](https://bitbucket.org/mamontovgrigory/ui/commit/7383ce06d9072f6aa8419b373e3dbe7bb589daed))

### [0.0.8](https://bitbucket.org/mamontovgrigory/ui/compare/v0.0.7...v0.0.8) (2020-10-13)

### Others

- release script ([cdbc86c](https://bitbucket.org/mamontovgrigory/ui/commit/cdbc86ce53177de2eb19d3c859761d2a3e90a9a3))

### [0.0.7](https://bitbucket.org/mamontovgrigory/ui/compare/v0.0.6...v0.0.7) (2020-10-13)

### Others

- yalc removed ([6e65e0a](https://bitbucket.org/mamontovgrigory/ui/commit/6e65e0a00c612c2762fe93cc023adec41f67a66d))

### [0.0.6](https://bitbucket.org/mamontovgrigory/ui/compare/v0.0.5...v0.0.6) (2020-10-02)

### Features

- conventional changelog ([bebed9e](https://bitbucket.org/mamontovgrigory/ui/commit/bebed9e5e43baeda9cab4a1e12757c318e8e4eea))

### Bug Fixes

- **grid:** filter text ([94825d5](https://bitbucket.org/mamontovgrigory/ui/commit/94825d538280d462ab1817da3ba3e1d993dd837a))

### [0.0.5](https://bitbucket.org/mamontovgrigory/ui/compare/v0.0.4...v0.0.5) (2020-10-02)

### [0.0.4](https://bitbucket.org/mamontovgrigory/ui/compare/v0.0.3...v0.0.4) (2020-10-02)

### [0.0.3](https://bitbucket.org/mamontovgrigory/ui/compare/v0.0.2...v0.0.3) (2020-10-02)

### Features

- standard version ([2ad79bb](https://bitbucket.org/mamontovgrigory/ui/commit/2ad79bb98d761bca832ba07ff7a94fae9f4b1c10))
