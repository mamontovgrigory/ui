import { createGlobalStyle } from '../src/services/styled';

import theme from '../src/services/theme';

export default createGlobalStyle`

  .rsg--root-1 {
    background-color: ${theme.colors.greyBg};
  }

  .rsg--sidebar-4 {
    background-color: ${theme.colors.darkBlue};
  }

  .rsg--link-41, .rsg--link-41:link, .rsg--link-41:visited {
    color: ${theme.colors.mainGrey};
    :hover {
      color: ${theme.colors.white};
    }
  }

  .rsg--logo-5 {
    border-bottom: none;
  }

  .rsg--logo-42 {
    color: ${theme.colors.mainGrey};
  }

  .rsg--content-3 {
    background-color: ${theme.colors.white};
  }

  .static {
    section {
      position: static !important;
    }
  }

  .mobile {
    width: 320px;
    section {
      position: static !important;
    }
  }

`;
