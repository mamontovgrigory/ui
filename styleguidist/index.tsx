import * as React from 'react';
import { ThemeProvider as StyledThemeProvider, ThemeProviderComponent } from 'styled-components';

import ResetCss from '../src/services/styles/resetCss';
import theme, { ITheme } from '../src/services/theme';

import StyleguidistCustomCss from './styles';

import { Helmet } from 'react-helmet';

interface IProps {
  children: React.ReactChild;
}

const CustomThemeProvider: ThemeProviderComponent<ITheme> = StyledThemeProvider;
const ThemeProvider: React.FC<IProps> = ({ children }) => {
  return (
    <CustomThemeProvider theme={theme}>
      <>
        <ResetCss />
        <Helmet />
        <StyleguidistCustomCss />
        {children}
      </>
    </CustomThemeProvider>
  );
};

export default ThemeProvider;
